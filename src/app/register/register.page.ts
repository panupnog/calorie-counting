import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';
import { ReqRegister } from '../service/api-user/interface/register';
import { RegisterService } from '../service/api-user/register.service';
import { AlertController } from '@ionic/angular';
import { LoadingService } from '../service/loading.service';
import { GlobalService } from '../service/global/global.service';
import { ReqLogin } from '../service/api-user/interface/login';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public dateBirthDay: string = moment().subtract('month').format('YYYY-MM-DD');
  public imgUser: any = null;
  public registerFrom = {
    email: '',
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    gender: '',
    height: null,
    weight: null,
    birthDay: '',
    target: null,
    role: 'user',
  };
  public targetOt = null;

  // ────────────────────────────────────────────────────────────────────────────────
  constructor(
    public navCtrl: NavController,
    private registerService: RegisterService,
    private alertController: AlertController,
    private loading: LoadingService,
    private global: GlobalService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.isSusses();
  }

  register() {
    // ─────────────────────────────────────────────────────────────────
    this.targetOt = 0;
    // ─────────────────────────────────────────────────────────────────
    const body: ReqRegister = {
      email: this.registerFrom.email,
      username: this.registerFrom.username,
      password: this.registerFrom.password,
      firstName: this.registerFrom.firstName,
      lastName: this.registerFrom.lastName,
      weight: this.registerFrom.weight,
      height: this.registerFrom.height,
      target: this.targetOt,
      gender: this.registerFrom.gender,
      role: this.registerFrom.role,
      birthday: this.dateBirthDay,
    };

    // console.log(body);
    this.registerService.register(body).subscribe((res) => {});

    // console.log('register success');

    const login: ReqLogin = {
      username: body.username,
      password: body.password,
    };

    // console.log('login-->', login);

    this.global.setLoginRegister(login);
    this.loading.registerSuccess();
  }
  // ────────────────────────────────────────────────────────────────────────────────

  // ────────────────────────────────────────────────────────────────────────────────
  isSusses() {
    if (
      (this.registerFrom.email &&
        this.registerFrom.username &&
        this.registerFrom.password &&
        this.registerFrom.firstName &&
        this.registerFrom.lastName &&
        this.registerFrom.gender &&
        this.registerFrom.height &&
        this.registerFrom.weight &&
        this.registerFrom.target !== '') ||
      (this.registerFrom.email &&
        this.registerFrom.username &&
        this.registerFrom.password &&
        this.registerFrom.firstName &&
        this.registerFrom.lastName &&
        this.registerFrom.gender &&
        this.registerFrom.height &&
        this.registerFrom.weight !== null)
    ) {
      return false;
    }

    return true;
  }

  base64Remove() {
    const imageName = 'userimg' + '.png';
    const imageBlob = this.dataURItoBlob(this.imgUser);
    const imageFile = new File([imageBlob], imageName, { type: 'image/png' });

    // console.log('imageFile-->', imageFile);
    // eslint-disable-next-line prefer-const
    let formData: FormData = new FormData();
    formData.append('image', imageFile);

    // console.log('formData-->', formData);
    // console.log('formData-->', typeof formData);
    // const id = this.getLocal.resData.id;
    // this.apiUser.uploadImageUser(formData, id).subscribe((res) => {
    //   console.log('res-->', res);
    // });
  }
  // ─────────────────────────────────────────────────────────────────────────────

  dataURItoBlob(dataURI: any) {
    const byteString = window.atob(
      dataURI.toString().replace(/^data:image\/(png|jpeg|jpg);base64,/, '')
    );
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/png' });
    return blob;
  }

  // ─────────────────────────────────────────────────────────────────────────────
  handleUpload(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgUser = reader.result;
    };
  }
  // ────────────────────────────────────────────────────────────────────────────────

  goBack() {
    this.navCtrl.navigateBack('/login');
  }
}
