import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabsPageModule } from './tabs/tabs.module';
import { InterceptorInterceptor } from './service/interceptors/interceptor.interceptor';
import { Camera } from '@awesome-cordova-plugins/camera/ngx';
import { FoodListPageModule } from './tabs/food-list/food-list.module';
import { BasketlistPageModule } from './tabs/basketlist/basketlist.module';
import { SharePipeModule } from './service/pipes/share-pipe/share-pipe.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    TabsPageModule,
    HttpClientModule,
    FoodListPageModule,
    BasketlistPageModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorInterceptor,
      multi: true,
    },
    Camera,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
