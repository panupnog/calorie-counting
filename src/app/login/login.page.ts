import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { ApiUsersService } from '../service/api-user/api-users.service';
import { ReqLogin, ResLogin } from '../service/api-user/interface/login';
import { AppService } from '../service/app.service';
import { GlobalService } from '../service/global/global.service';
import { LoadingService } from '../service/loading.service';
import { LocalService } from '../service/local.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: ReqLogin = {
    username: '',
    password: '',
  };
  public resData: ResLogin;
  public modal: any = null;
  // ────────────────────────────────────────────────────────────────────────────────
  constructor(
    public nav: NavController,
    private localService: LocalService,
    private appService: AppService,
    private loadingService: LoadingService,
    private apiUserService: ApiUsersService,
    private global: GlobalService
  ) {}

  ngOnInit() {
    if (this.modal !== null && this.modal !== undefined) {
      this.reqModalRegister();
    }
  }

  reqModalRegister() {
    this.modal = this.global.getLoginRegister();
    console.log('modal', this.modal);
  }
  // ────────────────────────────────────────────────────────────────────────────────
  async submitLogin() {
    await this.loadingService.showLoading();
    await this.appService.delay(500);

    const body: ReqLogin = {
      username: this.loginForm.username,
      password: this.loginForm.password,
    };
    console.log('body', body);

    this.apiUserService.login(body).subscribe(
      async (res) => {
        if (res && res.resCode === '0000') {
          this.resData = res;
          this.global.setResData(this.resData);
        }

        await this.loadingService.closeLoading();
        this.localService.setToken(res);
        await this.nav.navigateRoot('/tabs/home');
      },
      async (error) => {
        await this.loadingService.errorLogin();
        await this.loadingService.closeLoading();
      }
    );
  }
  // ────────────────────────────────────────────────────────────────────────────────
  navRegister() {
    this.nav.navigateForward('/register');
  }
}
