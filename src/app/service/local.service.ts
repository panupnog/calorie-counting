/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { ResLogin } from './api-user/interface/login';
import { resBasket } from './localinterface/interface/localstorage';

@Injectable({
  providedIn: 'root',
})
export class LocalService {
  constructor() {}

  setToken(data: ResLogin) {
    localStorage.setItem('resData', JSON.stringify(data));
  }
  setBasket(data: resBasket[]) {
    localStorage.setItem('basket', JSON.stringify(data));
  }
  setBadge(unit: number) {
    localStorage.setItem('unitBasket', unit.toString());
  }

  // ─────────────────────────────────────────────────────────────────────────────

  getToken(): ResLogin {
    const data = localStorage.getItem('resData');
    if (!data) {
      return;
    }
    const res: ResLogin = JSON.parse(data);
    return res;
  }

  getBasket(): resBasket[] {
    const data = localStorage.getItem('basket');
    if (!data) {
      return;
    }
    // const res: resBasket = JSON.parse(data);
    const res = JSON.parse(data);
    return res;
  }

  getBadge() {
    const data = localStorage.getItem('unitBasket');
    if (!data) {
      return;
    }
    return data;
  }

  removeAll() {
    localStorage.clear();
  }
}

export interface ResSetLocalStorage {
  data: Daum[];
}

export interface Daum {
  foodTypeName: string;
  datas: Data[];
}

export interface Data {
  foodTypeName: string;
  id: number;
  name: string;
  calorie: number;
  unit: number;
  uniteCal: number;
}
// ─────────────────────────────────────────────────────────────────────────────
