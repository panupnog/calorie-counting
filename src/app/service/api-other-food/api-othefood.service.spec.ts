import { TestBed } from '@angular/core/testing';

import { ApiOthefoodService } from './api-othefood.service';

describe('ApiOthefoodService', () => {
  let service: ApiOthefoodService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiOthefoodService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
