export interface ReqOtherFood {
  name: string;
  calorie: number;
  foodTypeId: number;
  otherCalorie: number;
}
// ─────────────────────────────────────────────────────────────────────────────

export interface ResOtherFood {
  resCode: string;
  msg: string;
  resData: ResData;
}

export interface ResData {
  id: number;
  name: string;
  calorie: number;
  foodTypeId: number;
  createdAt: string;
}
