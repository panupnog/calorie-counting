import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ReqOtherFood, ResOtherFood } from './interface/otherFood';

@Injectable({
  providedIn: 'root',
})
export class ApiOthefoodService {
  constructor(private http: HttpClient) {}

  otherFood(body: ReqOtherFood): Observable<ResOtherFood> {
    return this.http.post<ResOtherFood>(`${environment.url}/food`, body);
  }
}
