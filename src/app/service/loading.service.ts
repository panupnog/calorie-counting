import { Injectable } from '@angular/core';
import {
  AlertController,
  LoadingController,
  NavController,
} from '@ionic/angular';
import { ApiUsersService } from './api-user/api-users.service';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  private loading: HTMLIonLoadingElement = null;

  constructor(
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    public navCtrl: NavController,
    private appService: AppService,
    private apiUser: ApiUsersService
  ) {}
  // ────────────────────────────────────────────────────────────────────────────────

  async showLoading(msg?: string) {
    this.loading = await this.loadingCtrl.create({
      message: msg ? msg : 'เข้าสู่ระบบ...',
    });

    await this.loading.present();
  }
  // ────────────────────────────────────────────────────────────────────────────────

  async closeLoading() {
    if (this.loading !== null) {
      await this.loading.dismiss();
      this.loading = null;
    }
  }
  // ────────────────────────────────────────────────────────────────────────────────

  async errorLogin() {
    const alert = await this.alertController.create({
      header: 'เข้าสู่ระบบไม่สำเร็จ',
      subHeader: 'กรุณากรอกข้อมูลให้ถูกต้อง',
      buttons: ['OK'],
    });

    await alert.present();
  }
  // ────────────────────────────────────────────────────────────────────────────────
  async registerSuccess() {
    const alert = await this.alertController.create({
      header: 'ยืนยันการสมัคร',
      buttons: [
        {
          text: 'ไม่ใช่',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.navCtrl.navigateRoot('/login');
          },
        },
      ],
    });

    await alert.present();
  }
  // ────────────────────────────────────────────────────────────────────────────────

  async deleteUser() {
    const alert = await this.alertController.create({
      header: 'คุณแน่ใจหรือไม่',
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'ไม่ใช่',
          cssClass: 'alert-button-cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'ใช่',
          cssClass: 'alert-button-confirm',
          role: 'confirm',
          handler: () => {
            console.log('Confirm Okay');
            this.deleteSusses();
          },
        },
      ],
    });

    await alert.present();
  }

  async deleteSusses() {
    const alert = await this.alertController.create({
      header: 'ลบข้อมูลสำเร็จ',
      subHeader: 'บัญชีของคุณถูกลบแล้ว',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {
            console.log('deleteSusses ');
            this.appService.logout();
            this.navCtrl.navigateRoot('/login');
          },
        },
      ],
    });
    await alert.present();
  }

  // ─────────────────────────────────────────────────────────────────────────────

  async presentAlertSubmit2() {
    const alert = await this.alertController.create({
      header: 'แก้ไขข้อมูลสำเร็จ',
      subHeader: 'ข้อมูลของคุณถูกแก้ไขแล้ว',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {
            console.log('updateSusses ');
            this.navCtrl.navigateRoot('/tabs/home');
          },
        },
      ],
    });

    await alert.present();
  }
}
