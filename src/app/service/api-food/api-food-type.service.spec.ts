import { TestBed } from '@angular/core/testing';

import { ApiFoodTypeService } from './api-food-type.service';

describe('ApiFoodTypeService', () => {
  let service: ApiFoodTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiFoodTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
