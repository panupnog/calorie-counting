export interface ResFindAllFoodType {
  resCode: string;
  msg: string;
  resData: ResDataFood[];
}

export interface ResDataFood {
  id: number;
  foodTypeName: string;
  foodTypeImagePath: string;
  foodLists: FoodList[];
  isSelect?: boolean;
}

export interface FoodList {
  id: number;
  name: string;
  calorie: number;
  userId: string;
}
// ─────────────────────────────────────────────────────────────────────────────
export interface ReqPaginationFood {
  perPages: number;
  page: number;
  search: string;
}
export interface ResPaginationFood {
  resCode: string;
  msg: string;
  resData: ResData;
}

export interface ResData {
  itemsPerPage: number;
  totalItems: number;
  currentPage: number;
  totalPages: number;
  datas: Data[];
}

export interface Data {
  id: number;
  name: string;
  calorie: number;
  foodTypeId: number;
  unit: number;
}
// ─────────────────────────────────────────────────────────────────────────────

export interface ReqFoodId {
  foodId: number;
  unit: number;
}

export interface ResDataFoodId {
  result: number;
}
