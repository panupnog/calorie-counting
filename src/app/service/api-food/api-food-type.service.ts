import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  ReqFoodId,
  ReqPaginationFood,
  ResDataFoodId,
  ResFindAllFoodType,
  ResPaginationFood,
} from './interface/findAllFoodtype';

@Injectable({
  providedIn: 'root',
})
export class ApiFoodTypeService {
  constructor(private http: HttpClient) {}

  getFindAllFoodType(): Observable<ResFindAllFoodType> {
    return this.http.get<ResFindAllFoodType>(
      `${environment.url}/food-type/findAllFoodType`,
      {}
    );
  }

  paginationFood(x: ReqPaginationFood): Observable<ResPaginationFood> {
    return this.http.post<ResPaginationFood>(
      `${environment.url}/food/paginationFood`,
      x
    );
  }

  calById(body: ReqFoodId[]): Observable<ResDataFoodId> {
    return this.http.post<ResDataFoodId>(
      `${environment.url}/bmi/cal/{foodId}`,
      body
    );
  }
}
// ─────────────────────────────────────────────────────────────────────────────
