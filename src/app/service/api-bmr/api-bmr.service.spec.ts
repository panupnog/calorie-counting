import { TestBed } from '@angular/core/testing';

import { ApiBmrService } from './api-bmr.service';

describe('ApiBmrService', () => {
  let service: ApiBmrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiBmrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
