export interface ResBmr {
  result: number;
}
// ─────────────────────────────────────────────────────────────────────────────

export interface ResBmrResult {
  gender: string;
  result: number;
}
