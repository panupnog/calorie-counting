import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResBmr, ResBmrResult } from './interface/bmr-cal';

@Injectable({
  providedIn: 'root',
})
export class ApiBmrService {
  constructor(private http: HttpClient) {}

  getBmr(): Observable<ResBmrResult> {
    return this.http.get<ResBmrResult>(`${environment.url}/bmi/cal/bmr`, {});
  }
}
