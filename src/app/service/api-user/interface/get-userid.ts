export interface ResUserId {
  resCode: string;
  msg: string;
  resData: ResData;
}

export interface ResData {
  id: string;
  email: string;
  userName: string;
  firstName: string;
  lastName: string;
  image: string;
  role: string;
  status: boolean;
  gender: string;
  weight: number;
  height: number;
  target: number;
  targetCal: number;
  birthday: string;
}
