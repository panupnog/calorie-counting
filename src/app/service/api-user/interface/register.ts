export interface ReqRegister {
  email: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  weight: number;
  height: number;
  target: string;
  gender: string;
  role: string;
  birthday: string;
}
// ────────────────────────────────────────────────────────────────────────────────
