export interface ReqLogin {
  username: string;
  password: string;
}
// ────────────────────────────────────────────────────────────────────────────────
export interface ResLogin {
  resCode: string;
  msg: string;
  resData: ResData;
}

export interface ResData {
  id: string;
  email: string;
  username: string;
  firstName: string;
  lastName: string;
  role: string;
  status: boolean;
  gender: string;
  weight: number;
  height: number;
  target: string;
  birthday: string;
  accessToken: string;
  refreshToken: string;
  expire: string;
  password: string;
}
// ─────────────────────────────────────────────────────────────────────────────

export interface ResBmi {
  bmi: number;
  stateMent: string;
}

export interface ResDataTarget {
  resCode: string;
  msg: string;
  resData: DataTarget;
}

export interface DataTarget {
  id: string;
  email: string;
  userName: string;
  firstName: string;
  lastName: string;
  image: string;
  role: string;
  status: boolean;
  gender: string;
  weight: number;
  height: number;
  target: number;
  targetCal: any;
  birthday: string;
}
