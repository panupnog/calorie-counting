export interface ReqUserUpdate {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  weight: number;
  height: number;
  email: string;
}
// ─────────────────────────────────────────────────────────────────────────────
