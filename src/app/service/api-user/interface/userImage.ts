export interface ResUerImage {
  resCode: string;
  msg: string;
  resData: ResData;
}

export interface ResData {
  id: string;
  userImagePath: string;
}
