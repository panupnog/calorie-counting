import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResUserId } from './interface/get-userid';
import {
  ReqLogin,
  ResBmi,
  ResData,
  ResDataTarget,
  ResLogin,
} from './interface/login';
import { ResUerImage } from './interface/userImage';
import { ReqUserUpdate } from './interface/users-Update';

@Injectable({
  providedIn: 'root',
})
export class ApiUsersService {
  constructor(private http: HttpClient) {}

  login(body: ReqLogin): Observable<ResLogin> {
    return this.http.post<ResLogin>(`${environment.url}/users/login`, body);
  }

  usersUpdate(body: ReqUserUpdate, id: string): Observable<ResLogin> {
    return this.http.patch<ResLogin>(
      `${environment.url}/users/update/${id}`,
      body
    );
  }

  bmiCal(): Observable<ResBmi> {
    return this.http.get<ResBmi>(`${environment.url}/bmi/cal`, {});
  }

  usersById(id: string): Observable<ResUserId> {
    return this.http.get<ResUserId>(`${environment.url}/users/${id}`);
  }

  uploadImageUser(imagePath: FormData, id: string): Observable<ResUerImage> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'multipart/form-data');
    headers = headers.append('Accept', '*/*');

    return this.http.post<ResUerImage>(
      `${environment.url}/users/uploads-image/${id}`,
      imagePath,
      { headers }
    );
  }

  deleteUsers(id: string): Observable<any> {
    return this.http.delete<any>(`${environment.url}/users/del/${id}`);
  }

  findOneNotCache(id: string): Observable<ResDataTarget> {
    return this.http.get<ResDataTarget>(
      `${environment.url}/users/findOneNotCache/${id}`
    );
  }
}
