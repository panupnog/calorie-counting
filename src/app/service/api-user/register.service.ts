import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ReqRegister } from './interface/register';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  constructor(private http: HttpClient) {}

  register(body: ReqRegister): Observable<any> {
    return this.http.post<any>(`${environment.url}/users/register`, body);
  }
}
