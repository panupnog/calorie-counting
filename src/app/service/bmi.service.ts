import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResBmi } from './api-user/interface/login';

@Injectable({
  providedIn: 'root',
})
export class BmiService {
  constructor(private http: HttpClient) {}

  bmiCal(): Observable<ResBmi> {
    return this.http.get<ResBmi>(`${environment.url}/bmi/cal`, {});
  }
}
