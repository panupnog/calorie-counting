import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';
import { LocalService } from './local.service';
import { resBasket } from './localinterface/interface/localstorage';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  public dateNow = null;
  constructor(private localService: LocalService, private nav: NavController) {}

  delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  // ────────────────────────────────────────────────────────────────────────────────

  async logout() {
    console.log('logout');

    await this.nav.navigateRoot('/login');
    this.localService.removeAll();
  }
  // ────────────────────────────────────────────────────────────────────────────────

  convertNumber(val: string) {
    let value = 0;
    try {
      value = Number(val);
      return isNaN(value) ? 0 : value;
    } catch (error) {
      console.error(error);
    }
    return value;
  }
  // ────────────────────────────────────────────────────────────────────────────────

  time() {
    setInterval(() => {
      this.dateNow = moment().locale('th').format('lll');
    }, 1500);
  }

  // check basket
  async checkBasket() {
    if (await this.localService.getBasket()) {
      const temp = await this.localService.getBasket();
      const basketData = {
        basket: this.localService.getBasket(),
        isShow: true,
        basketCount: 0,
      };

      // for (const iterator of [temp]) {
      //   for (const item of iterator.datas) {
      //     basketData.basketCount = (await basketData.basketCount) + item.unit;
      //   }
      // }
      return basketData;
    }
  }
}
