export interface ResReport {
  id: number;
  height: number;
  weight: number;
  unit: number;
  calorie: number;
  foodName: string;
  bmiTotal: number;
  bmrTotal: number;
  isDelete: boolean;
  userId: string;
  otherFoodId: any;
  foodId: number;
  createdAt: string;
  updatedAt: string;
  foodList: FoodList;
}
// ─────────────────────────────────────────────────────────────────────────────

export interface FoodList {
  id: number;
  name: string;
  calorie: number;
  foodTypeId: number;
  userId: any;
  createdAt: string;
  updatedAt: string;
}
