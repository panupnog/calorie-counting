import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResReport } from './interface/report';

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor(private http: HttpClient) {}

  getReport(): Observable<ResReport[]> {
    return this.http.get<ResReport[]>(
      `${environment.url}/report/findAllResult`,
      {}
    );
  }

  deleteListReport(reportId: number): Observable<ResReport> {
    return this.http.delete<ResReport>(
      `${environment.url}/report/delete/${reportId}`
    );
  }
}
