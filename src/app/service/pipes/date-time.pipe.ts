import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateTime',
})
export class DateTimePipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): unknown {
    return moment(value).format('DD/MM/YYYY');
  }
}
