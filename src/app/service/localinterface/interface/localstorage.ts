/* eslint-disable @typescript-eslint/naming-convention */
export interface resBasket {
  foodTypeName: string;
  datas: listData[];
}

export interface listData {
  foodTypeName: string;
  id: number;
  name: string;
  calorie: number;
  unit: number;
  uniteCal: number;
}
