import { Injectable } from '@angular/core';
import { ReqLogin, ResLogin } from '../api-user/interface/login';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  public resData: ResLogin;
  public loginBody: ReqLogin;

  constructor() {}

  setResData(data: ResLogin) {
    this.resData = data;
  }
  setLoginRegister(data: ReqLogin) {
    this.loginBody = data;
  }

  // ────────────────────────────────────────────────────────────────────────────────

  getResData() {
    return this.resData;
  }

  getLoginRegister() {
    return this.loginBody;
  }
}
