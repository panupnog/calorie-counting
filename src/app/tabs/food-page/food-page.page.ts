import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AlertController,
  ModalController,
  NavController,
} from '@ionic/angular';
import * as moment from 'moment';
import { ApiFoodTypeService } from 'src/app/service/api-food/api-food-type.service';
import {
  ReqFoodId,
  ReqPaginationFood,
  ResDataFood,
  ResFindAllFoodType,
} from 'src/app/service/api-food/interface/findAllFoodtype';
import { ReportService } from 'src/app/service/api-report/report.service';
import { ResData } from 'src/app/service/api-user/interface/login';
import { AppService } from 'src/app/service/app.service';
import { GlobalService } from 'src/app/service/global/global.service';
import { LocalService } from 'src/app/service/local.service';
import { AddOtherFoodPage } from '../add-other-food/add-other-food.page';
import { BasketlistPage } from '../basketlist/basketlist.page';
import { FoodListPage } from '../food-list/food-list.page';

@Component({
  selector: 'app-food-page',
  templateUrl: './food-page.page.html',
  styleUrls: ['./food-page.page.scss'],
})
export class FoodPagePage implements OnInit, OnDestroy {
  // ────────────────────────────────────────────────────────────────────────────────ประกาศตัวแปร

  public dateNow = null;
  public foodPage = 1;
  public basketUnit = 0;
  public foodId = 0;
  public basket = true;
  public foodTypeImage = [];
  public foodTypeLabel = [];
  public foodTypeLabel2 = [];
  public unitNumbers = [];
  public resBasket: any = [];
  public paginationFood = [];
  public searchText = 'โค้ก';
  public foodTime = '';
  public baskets: any[] = [];
  public totalUnitAmount = 0;
  public totalCal = 0;
  public isEdit = false;

  // ────────────────────────────────────────────────────────────────────────────────

  constructor(
    private global: GlobalService,
    private nav: NavController,
    private apiFoodTypeService: ApiFoodTypeService,
    private modalController: ModalController,
    public localService: LocalService,
    private alertController: AlertController,
    public appService: AppService
  ) {}

  ngOnInit() {
    this.time();
    this.getFoodType();

    const getBasket = this.localService.getBasket();
    this.resBasket = getBasket;
    console.log('getBasket-->', this.resBasket);
  }
  ngOnDestroy() {}

  ionViewWillEnter() {
    this.countUnitBasKet();
  }

  countUnitBasKet() {
    const unit = localStorage.getItem('unitBasket');
    if (unit) {
      this.basketUnit = Number(unit);
    } else {
      this.basketUnit = 0;
    }
  }
  time() {
    setInterval(() => {
      this.dateNow = moment().locale('th').format('lll');
    }, 1500);
  }
  // ────────────────────────────────────────────────────────────────────────────────
  getFoodType() {
    // console.log('test');

    this.foodTypeLabel = [];
    this.apiFoodTypeService.getFindAllFoodType().subscribe(
      (res) => {
        console.log('res-->', res);
        if (res.resCode === '0000') {
          for (const data of res.resData) {
            const foodType: ResDataFood = {
              id: data.id,
              foodTypeName: data.foodTypeName,
              foodTypeImagePath: data.foodTypeImagePath,
              foodLists: data.foodLists,
              isSelect: false,
            };

            this.foodTypeLabel.push(foodType);
          }
          this.checkSelect();
        }
      },
      (err) => {
        console.error('errorGetFoodType-->', err);
      }
    );
  }

  cal() {
    this.totalCal = 0;
    for (const res of this.paginationFood) {
      res.uniteCal = res.unit * res.calorie;
    }

    for (const res of this.paginationFood) {
      this.totalCal = this.totalCal + res.uniteCal;
    }

    return this.totalCal;
  }

  calCart() {
    this.totalCal = 0;

    for (const res of this.foodTypeLabel) {
      for (const food of res.foodLists) {
        food.uniteCal = food.unit * food.calorie;
      }
    }
    for (const res of this.foodTypeLabel) {
      for (const food of res.foodLists) {
        this.totalCal = this.totalCal + food.uniteCal;
      }
    }

    for (const res of this.paginationFood) {
      this.totalCal = this.totalCal + res.uniteCal;
    }

    return this.totalCal;
  }

  // repairData() {
  //   const localBasketGet = localStorage.getItem('basket');
  //   let basket = null;
  //   if (localBasketGet) {
  //     basket = JSON.parse(localBasketGet);
  //   }

  //   console.log('basket-->', basket);

  //   if (basket) {
  //     console.log('basket-->', basket);
  //     const findBasket = basket.find((res) => res.id === this.data.id);
  //     this.isEdit = findBasket ? true : false;
  //     console.log('findBasket-->', findBasket);
  //     if (findBasket) {
  //       for (const data of this.data.foodLists) {
  //         const findData = findBasket.datas.find((res) => res.id === data.id);
  //         if (findData) {
  //           data.unit = findData.unit;
  //         }
  //       }
  //     }
  //   }

  //   this.foodLists = this.data.foodLists;
  // }

  confirm() {
    const res = this.paginationFood.filter((data) => data.unit > 0);
    // console.log('res-->', res);

    // const localBasketGet = localStorage.getItem('basket');

    // if (localBasketGet) {
    //   this.baskets = JSON.parse(localBasketGet);
    // }

    // this.basket = false;

    // const checkDupData = this.baskets.find((x) => x.id === id);
    // if (checkDupData) {
    //   let indexDelete = null;
    //   for (const [i, res] of this.baskets.entries()) {
    //     if (res.id === id) {
    //       if (data.data.length > 0) {
    //         res.datas = data.data;
    //       } else {
    //         indexDelete = i;
    //       }
    //       break;
    //     }
    //   }

    //   if (indexDelete !== null) {
    //     this.baskets.splice(indexDelete, 1);
    //   }
    // } else {
    //   this.baskets.push({
    //     // eslint-disable-next-line object-shorthand
    //     id: id,
    //     foodTypeName: data.data[0].foodTypeName,
    //     datas: data.data,
    //   });
    // }

    // const localBasket = JSON.stringify(this.baskets);
    // localStorage.setItem('basket', localBasket);

    // this.checkSelect();
    // this.basketsUnit();

    // const res = this.paginationFood.filter((data) => data.unit > 0);

    // const resData: any[] = [];
    // for (const data of res) {
    //   resData.push({
    //     id: data.id,
    //     name: data.name,
    //     calorie: data.calorie,
    //     unit: data.unit,
    //     uniteCal: data.uniteCal,
    //   });
    // }
    // console.log('resData-->', resData);
  }

  checkSelect() {
    const localBasketGet = localStorage.getItem('basket');

    if (localBasketGet) {
      this.baskets = JSON.parse(localBasketGet);
    }

    for (const [i, data] of this.foodTypeLabel.entries()) {
      const basket = this.baskets.find((x) => x.id === data.id);
      this.foodTypeLabel[i].isSelect = basket ? true : false;
    }

    // console.log('foodTypeLabel-->', this.foodTypeLabel);
  }

  async selectFood(id: number) {
    this.foodId = id;
    console.log('id-->', id);

    const foodData = this.foodTypeLabel.find((x) => x.id === id);
    const modal = this.modalController.create({
      component: FoodListPage,
      componentProps: {
        data: foodData,
      },
    });

    (await modal).present();

    (await modal).onWillDismiss().then((data) => {
      console.log('dataqw-->', data.data);
      if (data.data) {
        const localBasketGet = localStorage.getItem('basket');

        if (localBasketGet) {
          this.baskets = JSON.parse(localBasketGet);
        }
        this.basket = false;
        const checkDupData = this.baskets.find((x) => x.id === id);
        if (checkDupData) {
          let indexDelete = null;
          if (this.baskets.length > 0) {
            for (const [i, res] of this.baskets.entries()) {
              if (res.id === id) {
                if (data.data.length > 0) {
                  res.datas = data.data;
                } else {
                  indexDelete = i;
                }
                break;
              }
            }
          }

          if (indexDelete !== null) {
            this.baskets.splice(indexDelete, 1);
          }
        } else {
          this.baskets.push({
            // eslint-disable-next-line object-shorthand
            id: id,
            foodTypeName: data.data[0].foodTypeName,
            datas: data.data,
          });
        }
        const localBasket = JSON.stringify(this.baskets);
        localStorage.setItem('basket', localBasket);
        this.checkSelect();
        this.basketsUnit();
      }
    });
  }

  basketsUnit() {
    this.unitNumbers = [];
    for (const data1 of this.baskets) {
      for (const iterator of data1.datas) {
        this.unitNumbers.push(iterator.unit);
        this.basketUnit = this.unitNumbers.reduce(
          // eslint-disable-next-line id-blacklist
          (sum, number) => sum + number,
          0
        );
      }
    }
  }

  async selectBasket() {
    const localBasketGet = localStorage.getItem('basket');
    if (localBasketGet) {
      this.baskets = JSON.parse(localBasketGet);
    }

    const modal = this.modalController.create({
      component: BasketlistPage,
      componentProps: {
        data: this.baskets,
      },
    });

    (await modal).present();
    await (await modal).onWillDismiss().then(async (res) => {
      this.baskets = [];
      const localBasketGetBefore = localStorage.getItem('basket');
      if (!localBasketGetBefore) {
        for (const item of this.foodTypeLabel) {
          item.isSelect = false;
        }
      }
      const unit = localStorage.getItem('unitBasket');
      if (unit) {
        this.basketUnit = Number(unit);
      } else if (!unit) {
        this.basketUnit = 0;
      }
      this.checkSelect();
    });
  }

  async updateSearchResults() {
    this.paginationFood = [];
    const body: ReqPaginationFood = {
      perPages: 10,
      page: 1,
      search: this.searchText,
    };
    this.apiFoodTypeService.paginationFood(body).subscribe((res) => {
      console.log('res-->', res.resData);
      const dataPage = res.resData;
      for (const pageData of dataPage.datas) {
        Object.assign(pageData, { unit: 0 });
        Object.assign(pageData, { unitCal: 0 });
        this.paginationFood.push(pageData);
      }
      console.log('paginationFood-->', this.paginationFood);
    });
  }

  // ─────────────────────────────────────────────────────────────────────────────
  async addListFood() {
    const modal = this.modalController.create({
      component: AddOtherFoodPage,
    });
    (await modal).present();
    (await modal).onWillDismiss().then((data) => {
      this.foodTypeLabel = [];
      this.getFoodType();
    });
  }

  foodCheck() {
    console.log('this.foodTypeLabel-->', this.foodTypeLabel);
  }

  // ────────────────────────────────────────────────────────────────────────────────tabPage
  fPage() {
    this.foodPage = 1;
    console.log('foodPage-->', this.foodPage);
  }
  fPage2() {
    this.foodPage = 2;
    console.log('foodPage-->', this.foodPage);
  }
  fPage3() {
    this.foodPage = 3;
    console.log('foodPage-->', this.foodPage);
  }
  fPage4() {
    for (const item of this.foodTypeLabel) {
      for (const food of item.foodLists) {
        if (food.userId !== null) {
          Object.assign(item, { isUser: true });
        } else {
          Object.assign(item, { isUser: false });
        }
        Object.assign(food, { unit: 0 });
        Object.assign(food, { unitCal: 0 });
      }
    }
    this.foodPage = 4;
  }
  // ────────────────────────────────────────────────────────────────────────────────get foodTypeLabel
}
