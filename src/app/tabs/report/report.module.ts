import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportPageRoutingModule } from './report-routing.module';

import { ReportPage } from './report.page';
import { DateTimePipe } from 'src/app/service/pipes/date-time.pipe';
import { SharePipeModule } from 'src/app/service/pipes/share-pipe/share-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportPageRoutingModule,
    SharePipeModule,
  ],
  declarations: [ReportPage],
})
export class ReportPageModule {}
