import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ResReport } from 'src/app/service/api-report/interface/report';
import { ReportService } from 'src/app/service/api-report/report.service';
import { ResLogin } from 'src/app/service/api-user/interface/login';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {
  public dateNow = moment().locale('th').format('lll');
  public resReport: ResReport[] = [];
  public getLocal: ResLogin;
  public showFood = null;
  public dateStartToEnd: any = [];
  public dateStartToEndShow: any = [];
  public newResReport: NewReport[] = [];
  public sumBmr = [];
  public sumBmrShow = 0;
  public sumConst = [];
  public indexI = null;
  public calTest = 1776;
  public calTest2 = 0;
  constructor(private apiReport: ReportService) {}

  ngOnInit() {
    console.log('report');
  }

  // ตอนกดเข้ามาใน page จะทำการเรียกใช้งาน loadData()
  ionViewWillEnter() {
    this.loadData(this.indexI);
    this.getLocals();
  }
  getLocals() {
    this.getLocal = JSON.parse(localStorage.getItem('resData'));
    console.log(this.getLocal);
  }

  loadData(i: number) {
    this.indexI = i;
    // console.log('indexI-->', this.indexI);

    this.sumBmrShow = 0;
    this.newResReport = [];
    this.resReport = [];
    this.apiReport.getReport().subscribe(
      async (res) => {
        this.resReport = res.filter((item) => item.calorie);
        this.dateStartToEnd = this.resReport.map(
          (item) =>
            (item.createdAt = moment(item.createdAt).format('DD/MM/YYYY'))
        );
        if (this.dateStartToEnd.length > 0) {
          this.dateStartToEndShow =
            this.dateStartToEnd[this.dateStartToEnd.length - 1] +
            ' -' +
            'จนถึง' +
            ' -' +
            this.dateStartToEnd[0];

          // console.log('resReport-->', this.resReport);
        }

        for (const item of this.resReport) {
          if (this.newResReport.length === 0) {
            this.newResReport.push({
              date: item.createdAt,
              weight: item.weight,
              height: item.height,
              bmiTotal: item.bmiTotal,
              bmrTotal: item.bmrTotal,
              foodList: [],
              sumCal: this.sumBmrShow,
            });

            this.newResReport[0].foodList.push({
              id: item.foodId,
              foodName: item.foodName,
              calorie: item.calorie,
              unit: item.unit,
              userId: null,
              createdAt: item.createdAt,
              updatedAt: item.updatedAt,
              calorieTotal: 0,
            });
          } else {
            for (const newItem of this.newResReport) {
              if (newItem.date !== item.createdAt) {
                this.newResReport.push({
                  date: item.createdAt,
                  weight: item.weight,
                  height: item.height,
                  bmiTotal: item.bmiTotal,
                  bmrTotal: item.bmrTotal,
                  foodList: [],
                  sumCal: this.sumBmrShow,
                });
                continue;
              } else {
                newItem.foodList.push({
                  id: item.foodId,
                  foodName: item.foodName,
                  calorie: item.calorie,
                  unit: item.unit,
                  userId: null,
                  createdAt: item.createdAt,
                  updatedAt: item.updatedAt,
                  calorieTotal: 0,
                });
              }
            }
          }
        }
      },
      (err) => {
        console.error('err-->', err);
      }
    );
  }

  cal(i: number) {
    // return this.newResReport[i].foodList.reduce(
    //   (a, b) => a + b.calorie * b.unit,
    //   0
    // );
    this.calTest2 = this.newResReport[i].foodList.reduce(
      (a, b) => a + b.calorie * b.unit,
      0
    );
    return this.calTest2;
  }

  calNetive() {
    const x = this.calTest - this.calTest2;

    return x;
  }
  showFoodBefore(i: number) {
    if (this.showFood === i) {
      this.showFood = null;
      return;
    }
    this.showFood = i;
    // console.log('showFood-->', this.showFood);
  }
}

export interface NewReport {
  date: string;
  weight: number;
  height: number;
  bmiTotal: number;
  bmrTotal: number;
  foodList: FoodList[];
  sumCal: number;
}

export interface FoodList {
  id: number;
  foodName: string;
  calorie: number;
  // foodTypeId: number;
  unit: number;
  userId: any;
  createdAt: string;
  updatedAt: string;
  calorieTotal: number;
}
