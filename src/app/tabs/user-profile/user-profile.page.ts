import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { ApiUsersService } from 'src/app/service/api-user/api-users.service';
import { ResUserId } from 'src/app/service/api-user/interface/get-userid';
import { ResBmi, ResLogin } from 'src/app/service/api-user/interface/login';
import { BmiService } from 'src/app/service/bmi.service';
import { GlobalService } from 'src/app/service/global/global.service';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { ResUerImage } from 'src/app/service/api-user/interface/userImage';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit, OnDestroy {
  public dateNow = moment().locale('th').format('lll');
  public resData = null;
  public getLocal: ResLogin;
  public resUserId: ResUserId = null;
  public imgUser: any = null;
  public resUerImage = '';
  public btnSusses = false;
  public imgUserShow = '';
  public resCalBmi: ResBmi = {
    bmi: 0,
    stateMent: '',
  };
  public bmiStyle = '';
  public gander = {
    male: 'male',
    female: 'female',
  };
  public avatar = '';
  private resBmi = {
    thin: 'ผอมเกินไป',
    normal: 'น้ำหนักปกติ',
    over: 'น้ำหนักเกิน',
    fat: 'อ้วน',
  };

  constructor(
    private global: GlobalService,
    private alertController: AlertController,
    private nav: NavController,
    private bmiService: BmiService,
    private apiUser: ApiUsersService,
    private camera: Camera
  ) {}

  ngOnInit() {
    this.getLocal = JSON.parse(localStorage.getItem('resData'));
    if (this.getLocal.resCode === '0000') {
      this.userData();
    }
    this.calculatorBmi();
    this.calculatorBmi1();
  }
  ngOnDestroy() {
    if (this.getLocal !== null) {
      this.getLocal = null;
    }
    this.userData();
    console.log('getLocal-->', this.getLocal);
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.getLocal = JSON.parse(localStorage.getItem('resData'));
    if (this.getLocal.resCode === '0000') {
      this.userData();
    }
    this.calculatorBmi();
  }

  userData() {
    // ──────────────────────────────────────────────────────────────
    const id = this.getLocal.resData.id;
    this.apiUser.usersById(id).subscribe((res) => {
      this.resUserId = res;
      this.imgUserShow = this.resUserId.resData.image;
      console.log('imgUserShow-->', this.imgUserShow);
    });
  }

  editImg() {
    this.presentAlert();
  }

  base64Remove() {
    const imageName = 'userimg' + '.png';
    const imageBlob = this.dataURItoBlob(this.imgUser);
    const imageFile = new File([imageBlob], imageName, { type: 'image/png' });

    console.log('imageFile-->', imageFile);
    // eslint-disable-next-line prefer-const
    let formData: FormData = new FormData();
    formData.append('image', imageFile);

    console.log('formData-->', formData);
    console.log('formData-->', typeof formData);
    const id = this.getLocal.resData.id;
    this.apiUser.uploadImageUser(formData, id).subscribe((res) => {
      this.resUerImage = res.resData.userImagePath;
      console.log('res-->', this.resUerImage);
    });
  }

  calculatorBmi1() {
    this.bmiService.bmiCal().subscribe(
      (res) => {
        this.resCalBmi = res;
        console.log('resCalBmi-->', this.resCalBmi);

        const s = {
          weightdisplay: 'weightdisplay',
          weightdisplay2: 'weightdisplay2',
          weightdisplay3: 'weightdisplay3',
        };
        switch (this.resCalBmi.stateMent) {
          case this.resBmi.thin:
            this.bmiStyle = s.weightdisplay2;
            // console.log('caes1');
            if (this.resUserId.resData.gender === this.gander.male) {
              this.avatar = 'assets/img/cap.png';
            } else if (this.resUserId.resData.gender === this.gander.female) {
              this.avatar = 'assets/img/cap.png';
            }
            break;
          case this.resBmi.normal:
            // console.log('caes2');
            this.bmiStyle = s.weightdisplay;
            if (this.resUserId.resData.gender === this.gander.male) {
              this.avatar = 'assets/img/boy.png';
            } else if (this.resUserId.resData.gender === this.gander.female) {
              this.avatar = 'assets/img/mother.png';
            }
            break;
          case this.resBmi.over:
            // console.log('caes3');
            this.bmiStyle = s.weightdisplay3;
            if (this.resUserId.resData.gender === this.gander.male) {
              this.avatar = 'assets/img/nephew.png';
            } else if (this.resUserId.resData.gender === this.gander.female) {
              this.avatar = 'assets/img/girl2.png';
            }
            break;
          case this.resBmi.fat:
            this.bmiStyle = s.weightdisplay2;
            // console.log('caes4');
            if (this.resUserId.resData.gender === this.gander.male) {
              this.avatar = 'assets/img/User.png';
            } else if (this.resUserId.resData.gender === this.gander.female) {
              this.avatar = 'assets/img/girl.png';
            }
            break;
          default:
            // console.log('caseTakeABreath default');
            break;
        }
      },
      (err) => {
        console.error('errcalculatorBmi-->', err);
      }
    );
  }

  calculatorBmi() {
    this.bmiService.bmiCal().subscribe(
      (res) => {
        this.resCalBmi = res;
      },
      (err) => {
        console.error('errcalculatorBmi-->', err);
      }
    );
  }

  dataURItoBlob(dataURI: any) {
    const byteString = window.atob(
      dataURI.toString().replace(/^data:image\/(png|jpeg|jpg);base64,/, '')
    );
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/png' });
    return blob;
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'คุณแน่ใจหรือไม่ที่จะแก้ไขรูปภาพ',
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'ไม่ใช่',
          cssClass: 'alert-button-cancel',
          role: 'cancel',
          handler: () => {
            this.imgUser = null;
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'ใช่',
          cssClass: 'alert-button-confirm',
          role: 'confirm',
          handler: () => {
            console.log('editImg');
            this.base64Remove();
            this.btnSusses = true;
          },
        },
      ],
    });

    await alert.present();
  }

  handleUpload(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgUser = reader.result;
    };
  }

  goPage(page) {
    this.nav.navigateForward(page);
  }
}
