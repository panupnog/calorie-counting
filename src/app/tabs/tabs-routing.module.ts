import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from '../service/guard/auth-guard.guard';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        loadChildren: () =>
          import('./home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'food-page',
        loadChildren: () =>
          import('./food-page/food-page.module').then(
            (m) => m.FoodPagePageModule
          ),
      },
      {
        path: 'about',
        loadChildren: () =>
          import('./about/about.module').then((m) => m.AboutPageModule),
      },
      {
        path: 'report',
        loadChildren: () =>
          import('./report/report.module').then((m) => m.ReportPageModule),
      },
      {
        path: 'user-profile',
        loadChildren: () =>
          import('./user-profile/user-profile.module').then(
            (m) => m.UserProfilePageModule
          ),
      },
      {
        path: 'editdata-user',
        loadChildren: () =>
          import('./editdata-user/editdata-user.module').then(
            (m) => m.EditdataUserPageModule
          ),
      },
    ],
  },
  {
    path: 'food-list',
    loadChildren: () => import('./food-list/food-list.module').then( m => m.FoodListPageModule)
  },
  {
    path: 'basketlist',
    loadChildren: () => import('./basketlist/basketlist.module').then( m => m.BasketlistPageModule)
  },
  {
    path: 'add-other-food',
    loadChildren: () => import('./add-other-food/add-other-food.module').then( m => m.AddOtherFoodPageModule)
  },

  // {
  //   path: '**',
  //   redirectTo: '/tabs/home',
  //   pathMatch: 'full',
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
