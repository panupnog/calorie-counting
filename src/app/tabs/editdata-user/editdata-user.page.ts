import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { ApiUsersService } from 'src/app/service/api-user/api-users.service';
import { ResUserId } from 'src/app/service/api-user/interface/get-userid';
import { ResLogin } from 'src/app/service/api-user/interface/login';
import { ReqUserUpdate } from 'src/app/service/api-user/interface/users-Update';
import { AppService } from 'src/app/service/app.service';
import { LoadingService } from 'src/app/service/loading.service';

@Component({
  selector: 'app-editdata-user',
  templateUrl: './editdata-user.page.html',
  styleUrls: ['./editdata-user.page.scss'],
})
export class EditdataUserPage implements OnInit {
  public userTest = 'wave User';
  public getLocal: ResLogin;
  public resUserId: ResUserId = null;
  public updateUserModel: ReqUserUpdate = {
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    weight: null,
    height: null,
    email: '',
  };

  constructor(
    private nav: NavController,
    public appService: AppService,
    private loadingService: LoadingService,
    private alertController: AlertController,
    private apiUser: ApiUsersService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.getLocal = JSON.parse(localStorage.getItem('resData'));
    if (this.getLocal.resCode === '0000') {
      this.userData();
    }
  }

  userData() {
    const id = this.getLocal.resData.id;
    this.apiUser.usersById(id).subscribe((res) => {
      this.resUserId = res;
      if (this.resUserId !== null && this.resUserId !== undefined) {
        this.updateUserModel = {
          username: this.resUserId.resData.userName,
          password: '',
          firstName: this.resUserId.resData.firstName,
          lastName: this.resUserId.resData.lastName,
          weight: this.resUserId.resData.weight,
          height: this.resUserId.resData.height,
          email: this.resUserId.resData.email,
        };
      }
    });
  }

  isSusses() {
    if (
      this.updateUserModel.password === '' ||
      this.updateUserModel.password.length < 4
    ) {
      return true;
    } else {
      return false;
    }
  }

  userUpdate() {
    const id = this.getLocal.resData.id;
    const z: ReqUserUpdate = {
      username: this.updateUserModel.username,
      password: this.updateUserModel.password,
      firstName: this.updateUserModel.firstName,
      lastName: this.updateUserModel.lastName,
      weight: this.updateUserModel.weight,
      height: this.updateUserModel.height,
      email: this.updateUserModel.email,
    };

    // console.log('z', z);
    // console.log('id', id);

    this.apiUser.usersUpdate(z, id).subscribe((res) => {
      // console.log('usersUpdate-->', res);
    });
  }

  submit() {
    // console.log('submit');
    this.presentAlertSubmit1();
  }
  // ───────────────────────────────────────────────────────────────────────────── alert edit susses

  async presentAlertSubmit1() {
    const alert = await this.alertController.create({
      header: 'ยืนยันการแก้ไขข้อมูล',
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'ไม่ใช่',
          cssClass: 'alert-button-cancel',
          role: 'cancel',
          handler: () => {
            // console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'ใช่',
          cssClass: 'alert-button-confirm',
          role: 'confirm',
          handler: () => {
            // console.log('Confirm Okay');
            this.userUpdate();
            this.loadingService.presentAlertSubmit2();
          },
        },
      ],
    });

    await alert.present();

    // ────────────────────────────────────────────────────────────── alert delete
  }
  async alertDeleteUsers() {
    const alert = await this.alertController.create({
      header: 'ต้องการลบบัญชีผู้ใช้หรือไม่',
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'ไม่ใช่',
          cssClass: 'alert-button-cancel',
          role: 'cancel',
          handler: () => {
            // console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'ใช่',
          cssClass: 'alert-button-confirm',
          role: 'confirm',
          handler: () => {
            // console.log('Confirm Okay');
            this.alertUserSusses();
          },
        },
      ],
    });

    await alert.present();
  }

  async alertUserSusses() {
    const alert = await this.alertController.create({
      header: 'ลบบัญชีผู้ใช้สำเร็จ',
      subHeader: 'บัญชีผู้ใช้ถูกลบแล้ว',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {
            const id = this.getLocal.resData.id;
            this.apiUser.deleteUsers(id).subscribe((res) => {
              // console.log('deleteUsers-->', res);
            });
            this.appService.logout();
          },
        },
      ],
    });

    await alert.present();
  }

  // ─────────────────────────────────────────────────────────────────────────────
}
