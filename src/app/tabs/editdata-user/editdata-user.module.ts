import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditdataUserPageRoutingModule } from './editdata-user-routing.module';

import { EditdataUserPage } from './editdata-user.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditdataUserPageRoutingModule
  ],
  declarations: [EditdataUserPage]
})
export class EditdataUserPageModule {}
