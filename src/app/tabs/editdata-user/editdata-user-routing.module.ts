import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditdataUserPage } from './editdata-user.page';

const routes: Routes = [
  {
    path: '',
    component: EditdataUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditdataUserPageRoutingModule {}
