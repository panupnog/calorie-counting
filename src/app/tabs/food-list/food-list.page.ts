/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { AppService } from 'src/app/service/app.service';
import { LocalService } from 'src/app/service/local.service';

@Component({
  selector: 'app-food-list',
  templateUrl: './food-list.page.html',
  styleUrls: ['./food-list.page.scss'],
})
export class FoodListPage implements OnInit {
  @Input() data: any;
  public dateNow = null;
  public foodId = 0;
  public totalCal = 0;
  public foodTypeImage = [];

  public dataAutocomplete = [];
  public autocompleteInput = [];
  public isItemAvailable = false;
  public items = [];
  public isEdit = false;
  public foodLists = null;
  public typeCurrent = 'all';

  public dt = [];

  constructor(
    private modalController: ModalController,
    public localStorage: LocalService,
    private appService: AppService
  ) {}

  async ngOnInit() {
    this.time();
    console.log('datalist-->', this.data);

    for (const dataFood of this.data.foodLists) {
      Object.assign(dataFood, { unit: 0 });
      Object.assign(dataFood, { unitCal: 0 });
    }

    console.log(
      'this.localStorage.getBasket()-->',
      this.localStorage.getBasket()
    );

    this.dt = this.localStorage.getBasket();
    if (this.data.length > 0) {
      for (const [i, iterator] of this.dt.entries()) {
        console.log('iterator-->', i, ': ', iterator);
      }
    }

    this.repairData();
  }

  repairData() {
    const localBasketGet = localStorage.getItem('basket');
    let basket = null;
    if (localBasketGet) {
      basket = JSON.parse(localBasketGet);
    }

    if (basket) {
      console.log('basket-->', basket);
      const findBasket = basket.find((res) => res.id === this.data.id);
      this.isEdit = findBasket ? true : false;
      console.log('findBasket-->', findBasket);
      if (findBasket) {
        for (const data of this.data.foodLists) {
          const findData = findBasket.datas.find((res) => res.id === data.id);
          if (findData) {
            data.unit = findData.unit;
          }
        }
      }
    }

    this.foodLists = this.data.foodLists;
  }

  filter(type: string) {
    this.typeCurrent = type;
    this.repairData();
  }

  time() {
    setInterval(() => {
      this.dateNow = moment().locale('th').format('lll');
    }, 1500);
  }

  selectFood(id: number) {
    this.foodId = id;
  }

  updateSearchResults(event) {
    this.data.foodLists = this.foodLists.filter((res) =>
      res.name.toLowerCase().match(event.target.value.toLowerCase())
    );
  }
  closePage() {
    this.modalController.dismiss();
  }

  cal() {
    this.totalCal = 0;
    for (const res of this.data.foodLists) {
      res.uniteCal = res.unit * res.calorie;
    }

    for (const res of this.data.foodLists) {
      this.totalCal = this.totalCal + res.uniteCal;
    }

    return this.totalCal;
  }

  confirm() {
    const res = this.data.foodLists.filter((data) => data.unit > 0);

    const resData: any[] = [];
    for (const data of res) {
      resData.push({
        foodTypeName: this.data.foodTypeName,
        id: data.id,
        name: data.name,
        calorie: data.calorie,
        unit: data.unit,
        uniteCal: data.uniteCal,
      });
    }

    this.modalController.dismiss(resData);
  }
}

interface FoodItem {
  name: string;
}
