import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddOtherFoodPageRoutingModule } from './add-other-food-routing.module';

import { AddOtherFoodPage } from './add-other-food.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddOtherFoodPageRoutingModule
  ],
  declarations: [AddOtherFoodPage]
})
export class AddOtherFoodPageModule {}
