import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { ApiFoodTypeService } from 'src/app/service/api-food/api-food-type.service';
import { ResDataFood } from 'src/app/service/api-food/interface/findAllFoodtype';
import { ApiOthefoodService } from 'src/app/service/api-other-food/api-othefood.service';
import { ReqOtherFood } from 'src/app/service/api-other-food/interface/otherFood';

@Component({
  selector: 'app-add-other-food',
  templateUrl: './add-other-food.page.html',
  styleUrls: ['./add-other-food.page.scss'],
})
export class AddOtherFoodPage implements OnInit {
  public dateNow = null;
  public foodTypeLabel = [];
  public reqOtherFood: ReqOtherFood = {
    name: '',
    calorie: 0,
    foodTypeId: 0,
    otherCalorie: null,
  };
  constructor(
    private modalController: ModalController,
    private apiFoodTypeService: ApiFoodTypeService,
    private apiOthefoodService: ApiOthefoodService,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.time();
    this.getFoodType();
  }

  closePage() {
    this.modalController.dismiss();
  }

  time() {
    setInterval(() => {
      this.dateNow = moment().locale('th').format('lll');
    }, 1500);
  }

  getFoodType() {
    this.apiFoodTypeService.getFindAllFoodType().subscribe(
      (res) => {
        // console.log('res-->', res);
        if (res.resCode === '0000') {
          for (const data of res.resData) {
            const foodType: ResDataFood = {
              id: data.id,
              foodTypeName: data.foodTypeName,
              foodTypeImagePath: data.foodTypeImagePath,
              foodLists: data.foodLists,
              isSelect: false,
            };

            this.foodTypeLabel.push(foodType);
          }
          // this.checkSelect();
        }
      },
      (err) => {
        // console.error('errorGetFoodType-->', err);
      }
    );
  }
  isCheck() {
    if (
      this.reqOtherFood.foodTypeId === 0 &&
      this.reqOtherFood.name === '' &&
      this.reqOtherFood.otherCalorie === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  async submit() {
    const alert = await this.alertController.create({
      header: 'ยืนยันการเพิ่มรายการอาหาร!',
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'ยกเลิก',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'ตกลง',
          cssClass: 'alert-button-confirm',
          handler: async () => {
            const body: ReqOtherFood = {
              name: this.reqOtherFood.name,
              calorie: this.reqOtherFood.calorie,
              foodTypeId: this.reqOtherFood.foodTypeId,
              otherCalorie: 0,
            };

            // console.log('body-->', body);
            this.apiOthefoodService.otherFood(body).subscribe(
              async (res) => {
                console.log('res-->', res);
                await this.modalController.dismiss();
              },
              (err) => {
                console.log('err-->', err);
              }
            );
          },
        },
      ],
    });

    await alert.present();
  }
}
