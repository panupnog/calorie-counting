import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddOtherFoodPage } from './add-other-food.page';

const routes: Routes = [
  {
    path: '',
    component: AddOtherFoodPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddOtherFoodPageRoutingModule {}
