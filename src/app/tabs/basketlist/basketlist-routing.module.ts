import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasketlistPage } from './basketlist.page';

const routes: Routes = [
  {
    path: '',
    component: BasketlistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BasketlistPageRoutingModule {}
