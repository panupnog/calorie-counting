import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiFoodTypeService } from 'src/app/service/api-food/api-food-type.service';
import { ReqFoodId } from 'src/app/service/api-food/interface/findAllFoodtype';
import {
  LocalService,
  ResSetLocalStorage,
} from 'src/app/service/local.service';
import {
  listData,
  resBasket,
} from 'src/app/service/localinterface/interface/localstorage';
import { FoodListPage } from '../food-list/food-list.page';

@Component({
  selector: 'app-basketlist',
  templateUrl: './basketlist.page.html',
  styleUrls: ['./basketlist.page.scss'],
})
export class BasketlistPage implements OnInit {
  @Input() data: any[];
  public foodTypeNameTitle = '';
  public dataReqFoodId = [];
  public totalCal = 0;
  public listBasket = [];
  public resBasket: any = [];
  public baskets: any[] = [];
  public basket = false;
  public unitNumbers = [];
  public basketUnit = 0;

  constructor(
    private modalController: ModalController,
    private localService: LocalService,
    private apiFoodTypeService: ApiFoodTypeService
  ) {}

  async ngOnInit() {
    console.log('data-->', this.data);

    // const body: ResSetLocalStorage = {
    //   data: this.data,
    // };
    // console.log('data-->', this.data);
    let amountUnit = 0;
    for (const iterator of this.data) {
      for (const item of iterator.datas) {
        amountUnit = amountUnit + item.unit;
      }
    }
    this.localService.setBadge(amountUnit);
    await this.localService.setBasket(this.data);
  }

  async ionViewWillEnter() {
    const getBasket = await this.localService.getBasket();
    this.resBasket = getBasket;
    // console.log('getBasket-->', this.resBasket);
  }

  closePage() {
    this.modalController.dismiss();
  }

  cal() {
    this.totalCal = 0;
    for (const res of this.resBasket) {
      for (const res2 of res.datas) {
        res2.uniteCal = res2.unit * res2.calorie;
      }
    }

    for (const res of this.resBasket) {
      for (const res2 of res.datas) {
        this.totalCal = this.totalCal + res2.uniteCal;
      }
    }

    return this.totalCal;
  }
  checkSelect() {
    const localBasketGet = localStorage.getItem('basket');

    if (localBasketGet) {
      this.baskets = JSON.parse(localBasketGet);
    }

    for (const data of this.resBasket) {
      const basket = this.baskets.find((x) => x.id === data.id);
      data.isSelect = basket ? true : false;
    }

    // console.log('foodTypeLabel-->', this.resBasket);
  }
  basketsUnit() {
    this.unitNumbers = [];
    for (const data1 of this.baskets) {
      for (const iterator of data1.datas) {
        this.unitNumbers.push(iterator.unit);
        this.basketUnit = this.unitNumbers.reduce(
          // eslint-disable-next-line id-blacklist
          (sum, number) => sum + number,
          0
        );
      }
    }
  }

  async editBasket(id: number) {
    // console.log('id-->', id);
    const foodData = this.resBasket.find((x) => x.id === id);
    const modal = this.modalController.create({
      component: FoodListPage,
      componentProps: {
        data: foodData,
      },
    });
    (await modal).present();

    (await modal).onWillDismiss().then((data) => {
      // console.log('data-->', data.data);
      if (data.data) {
        const localBasketGet = localStorage.getItem('basket');
        // console.log('localBasketGet-->', localBasketGet);

        if (localBasketGet) {
          this.baskets = JSON.parse(localBasketGet);
        }
        this.basket = false;

        const checkDupData = this.baskets.find((x) => x.id === id);
        if (checkDupData) {
          let indexDelete = null;
          for (const [i, res] of this.baskets.entries()) {
            if (res.id === id) {
              if (data.data.length > 0) {
                res.datas = data.data;
              } else {
                indexDelete = i;
              }
              break;
            }
          }

          if (indexDelete !== null) {
            this.baskets.splice(indexDelete, 1);
          }
        } else {
          this.baskets.push({
            // eslint-disable-next-line object-shorthand
            id: id,
            foodTypeName: data.data[0].foodTypeName,
            datas: data.data,
          });
        }

        const localBasket = JSON.stringify(this.baskets);
        localStorage.setItem('basket', localBasket);

        this.checkSelect();
        this.basketsUnit();
      }
    });
  }

  submit() {
    const datalist = this.resBasket;
    for (const basget of datalist) {
      for (const iterator of basget.datas) {
        const body: ReqFoodId = {
          foodId: iterator.id,
          unit: iterator.unit,
        };

        this.dataReqFoodId.push(body);
        // console.log('dataReqFoodId-->', this.dataReqFoodId);
        this.apiFoodTypeService.calById(this.dataReqFoodId).subscribe(
          async (res) => {
            this.data = [];
            localStorage.removeItem('basket');
            localStorage.removeItem('unitBasket');
            await this.modalController.dismiss({ clear: true });
          },
          async (err) => {
            console.error('err-->', err);
            await this.modalController.dismiss({ clear: false });
          }
        );
      }
    }
  }
}
