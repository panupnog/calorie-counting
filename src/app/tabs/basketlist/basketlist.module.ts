import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BasketlistPageRoutingModule } from './basketlist-routing.module';

import { BasketlistPage } from './basketlist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BasketlistPageRoutingModule
  ],
  declarations: [BasketlistPage]
})
export class BasketlistPageModule {}
