import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { ApiBmrService } from 'src/app/service/api-bmr/api-bmr.service';
import { ReportService } from 'src/app/service/api-report/report.service';
import { ApiUsersService } from 'src/app/service/api-user/api-users.service';
import { ResUserId } from 'src/app/service/api-user/interface/get-userid';
import { ResBmi, ResLogin } from 'src/app/service/api-user/interface/login';
import { BmiService } from 'src/app/service/bmi.service';
import { GlobalService } from 'src/app/service/global/global.service';
import { LocalService } from 'src/app/service/local.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public getLocal: ResLogin;
  public dateNow = null;
  public yearNow = moment().format('YYYY');
  public slideOpts = {
    slidesPerView: 1.5,
    initialSlide: 1,
    speed: 400,
    loop: true,
    centeredSlides: true,
    autoplay: true,
  };
  public foodTime = '';
  public resUserId: ResUserId = null;
  public resCalBmi: ResBmi = {
    bmi: 0,
    stateMent: '',
  };
  public resCalBmiS = '';
  public bmi = '';
  public height = null;
  public weight = null;
  public resultBmi = null;
  public statement = '';
  public birthday = null;
  public gander = {
    male: 'male',
    female: 'female',
  };
  public avatar = '';
  public bmiStyle = '';
  public bmr = 0;

  private resBmi = {
    thin: 'ผอมเกินไป',
    normal: 'น้ำหนักปกติ',
    over: 'น้ำหนักเกิน',
    fat: 'อ้วน',
  };

  // ────────────────────────────────────────────────────────────────────────────────

  constructor(
    private menu: MenuController,
    private apiUser: ApiUsersService,
    private global: GlobalService,
    private nav: NavController,
    private localService: LocalService,
    private bmiService: BmiService,
    private apiBmrService: ApiBmrService,
    private reportService: ReportService
  ) {}
  ngOnInit() {
    this.loadData();
    this.calculatorBmi();
    this.time();
    this.bmrCal();
  }

  ionViewWillEnter() {
    this.loadData();
    this.calculatorBmi();
    this.bmrCal();
    this.userData();
  }

  loadData() {
    this.getLocal = JSON.parse(localStorage.getItem('resData'));
    if (this.getLocal.resCode === '0000') {
      this.userData();
    }
  }
  // ─────────────────────────────────────────────────────────────────────────────
  time() {
    setInterval(() => {
      this.dateNow = moment().locale('th').format('lll');
    }, 1500);
  }
  // ──────────────────────────────────────────────────────────────
  userData() {
    const id = this.getLocal.resData.id;

    this.apiUser.usersById(id).subscribe((res) => {
      this.resUserId = res;
      // console.log('resUserId-->', this.resUserId);
    });

    // ────────────────────────────────────────────────────────────────────────────────
    // this.apiBmrService.getBmr().subscribe((res) => {
    //   this.bmr = res.result.toFixed();
    //   // console.log('resBmr-->', this.bmr);
    // });
  }

  calculatorBmi() {
    this.bmiService.bmiCal().subscribe(
      (res) => {
        this.resCalBmi = res;
        const s = {
          weightdisplay: 'weightdisplay',
          weightdisplay2: 'weightdisplay2',
          weightdisplay3: 'weightdisplay3',
        };
        switch (this.resCalBmi.stateMent) {
          case this.resBmi.thin:
            this.bmiStyle = s.weightdisplay2;
            // console.log('caes1');
            if (this.resUserId.resData.gender === this.gander.male) {
              this.avatar = 'assets/img/cap.png';
            } else if (this.resUserId.resData.gender === this.gander.female) {
              this.avatar = 'assets/img/cap.png';
            }
            break;
          case this.resBmi.normal:
            // console.log('caes2');
            this.bmiStyle = s.weightdisplay;
            if (this.resUserId.resData.gender === this.gander.male) {
              this.avatar = 'assets/img/boy.png';
            } else if (this.resUserId.resData.gender === this.gander.female) {
              this.avatar = 'assets/img/mother.png';
            }
            break;
          case this.resBmi.over:
            // console.log('caes3');
            this.bmiStyle = s.weightdisplay3;
            if (this.resUserId.resData.gender === this.gander.male) {
              this.avatar = 'assets/img/nephew.png';
            } else if (this.resUserId.resData.gender === this.gander.female) {
              this.avatar = 'assets/img/girl2.png';
            }
            break;
          case this.resBmi.fat:
            this.bmiStyle = s.weightdisplay2;
            // console.log('caes4');
            if (this.resUserId.resData.gender === this.gander.male) {
              this.avatar = 'assets/img/User.png';
            } else if (this.resUserId.resData.gender === this.gander.female) {
              this.avatar = 'assets/img/girl.png';
            }
            break;
          default:
            // console.log('caseTakeABreath default');
            break;
        }
      },
      (err) => {
        console.error('errcalculatorBmi-->', err);
      }
    );
  }

  bmrCal() {
    const userId = this.getLocal.resData.id;
    console.log('userId-->', userId);

    this.apiUser.findOneNotCache(userId).subscribe((res) => {
      console.log('resBmr-->', res);

      this.bmr = res.resData.target + res.resData.targetCal;

      // if (res.resData.targetCal === 0) {
      //   this.reportService.getReport().subscribe((res2) => {
      //     if (res2.length === 0 && res.resData.targetCal === 0) {
      //       this.bmr = res.resData.target;
      //       console.log('case1');
      //     }
      //   });
      // }
      // // if (res.resData.target === res.resData.targetCal) {
      // //   this.bmr = res.resData.targetCal;
      // //   console.log('case2');
      // // }
      // if (res.resData.targetCal !== 0) {
      //   this.bmr = res.resData.target + res.resData.targetCal;
      //   console.log('bmr-->', this.bmr);
      //   console.log('case3');
      // }
    });
  }
  // ────────────────────────────────────────────────────────────────────────────────btn foodTime

  goPageFood() {
    this.nav.navigateForward('tabs/food-page');
  }
  goPageReport() {
    this.nav.navigateForward('tabs/report');
  }

  // ────────────────────────────────────────────────────────────────────────────────
}
