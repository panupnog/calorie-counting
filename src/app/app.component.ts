import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppService } from './service/app.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(public appService: AppService, private nav: NavController) {}

  goPage(page) {
    this.nav.navigateForward(page);
  }
}
